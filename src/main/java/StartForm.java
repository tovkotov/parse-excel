import lombok.SneakyThrows;
import model.WorkDay;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;

public class StartForm extends JFrame {
    private JPanel mainPanel;
    private JButton exploreButton;
    private JButton importButton;
    private JButton exitButton;
    private JPanel buttonPanel;
    private JPanel statusPanel;
    private JLabel fileNameLabel;
    private JLabel dateLabel;
    private JProgressBar progressBar1;
    private JLabel statusLabel;

    private JFileChooser fileChooser = new JFileChooser();
    private String excelFileFromBZ;
    private ParseExcel parseExcel;

    public StartForm(String title) {
        super(title);

        add(getMainPanel());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(600, 230);
        setLocationRelativeTo(null);
        setVisible(true);

        exitButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        exploreButton.addActionListener(new AbstractAction() {
            @SneakyThrows
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser.setDialogTitle("Выбор файла");
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int result = fileChooser.showOpenDialog(mainPanel);
                if (result == JFileChooser.APPROVE_OPTION) {
                    setFileNameLabelText();
                    setDateLabelText();
                }
            }
        });

        importButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importButton.setEnabled(false);
                exitButton.setEnabled(false);
                exploreButton.setEnabled(false);
                saveToDataBase();
            }
        });
    }

    private void setDateLabelText() throws IOException {
        getDateFromFile();
        dateLabel.setText(parseExcel.dateRange);
    }

    private void setFileNameLabelText() {
        fileNameLabel.setText(fileChooser.getName(fileChooser.getSelectedFile()));
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private String getDateFromFile() throws IOException {
        excelFileFromBZ = fileChooser.getSelectedFile().getAbsolutePath();
        parseExcel = new ParseExcel(excelFileFromBZ);
        parseExcel.readFromExcel();
        LocalDate date = parseExcel.getCheckDate();

        if (Objects.equals(parseExcel.status, "")) {
            if (parseExcel.checkDate()) {
                importButton.setEnabled(true);
                statusLabel.setText("соединение с БД установлено, импорт возможен");
            }
        } else {
            statusLabel.setText(parseExcel.status);
            exitButton.setEnabled(true);
            //exploreButton.setEnabled(true);
        }

        return date.toString();
    }

    private void saveToDataBase() {
        SwingWorker<Boolean, Void> worker = new SwingWorker<>() {
            @Override
            protected Boolean doInBackground() {
                TreeSet<String> employeeList;
                ArrayList<WorkDay> workDays;

                StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure("hibernate.cfg.xml").build();
                Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
                SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
                Session session = sessionFactory.openSession();

                parseExcel.readFromExcel();
                employeeList = parseExcel.getEmployees();
                workDays = parseExcel.getWorkDays();

                progressBar1.setMinimum(0);
                progressBar1.setMaximum(workDays.size());

                int i = 0;
                for (WorkDay workDay : workDays) {
                    progressBar1.setValue(i++);
                    session.save(workDay);
                }

                return true;
            }

            protected void done() {
                boolean status;
                try {
                    status = get();
                    statusLabel.setText("Импорт завершен: " + parseExcel.getWorkDays().size() + " записей добавлено");
                    exitButton.setEnabled(true);
                    exploreButton.setEnabled(true);
                } catch (InterruptedException e) {
                    // This is thrown if the thread's interrupted.
                } catch (ExecutionException e) {
                    // This is thrown if we throw an exception
                    // from doInBackground.
                }
            }
        };
        worker.execute();
    }
}


